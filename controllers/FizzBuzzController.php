<?php 

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;


class FizzBuzzController extends Controller {
    public function multipleDe3($number) {
        if ($number % 3 == 0) {
            return "fizz";
        }
    }

    public function multipleDe5($number) {
        if ($number % 5 == 0) {
            return "buzz";
        }
    }

    public function multipleDe3Et5($number) {
        if ($number % 3 == 0 && $number % 5 == 0) {
            return "fizzbuzz";
        }
    }

    public function pasMultipleDe3Et5($number) {
        if (!$number % 3 == 0 || !$number % 5 == 0) {
            return "";
        }
    }

    public function StartGame($number) {
        for ($i = 0; $i <= $number; $i++) {
            if ($i % 3 == 0 || $i % 5 == 0) {
                $this->multipleDe3Et5($i);
                $this->multipleDe3($i);
                $this->multipleDe5($i);
            } else {
                return $number;
            }
        }
    }
}