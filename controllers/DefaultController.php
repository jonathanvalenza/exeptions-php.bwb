<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;

use Exception;
/**
 * Ceci est un exemple de contrôleur 
 * il charge le security middleware dans le constructeur
 * 
 *
 * @author loic
 */
class DefaultController extends Controller {

    /**
     * Le constructeur de la classe Controller charge les datas passées par le client,
     * Pour charger le security middleware, le contrôleur concret invoque la methode
     * @see \BWB\Framework\mvc\Controller::securityLoader() 
     * pour charger la couche securité.
     */
    function __construct() {
        parent::__construct();
        $this->securityLoader();
    }

    public function updateLog(string $message) {
        $log = fopen("./log.txt", "a");
        fwrite($log, date("j-F-Y H:i:s : ") . $message);
        fclose($log);

    }

    /**
     * Retourne la vue default.php qui se trouve dans le dossier views.
     * ici on traite l'exception
     * 
     * @link / methode invoquée lors d'une requête à la racine de l'application
     */
    public function getDefault() {
        $dao = new DAODefault();
        // var_dump($dao->getAll());
        try {
            var_dump($dao->getAll());
        } catch (Exception $ex) {
            echo "Une exception a été levé : <br>";
            echo $ex->getMessage();
            $this->updateLog($ex->getMessage());
        }

        // var_dump($dao->getAll());
        // $this->render("default");
    }

    public function getDefaultBy($id) {
        $dao = new DAODefault();
        // var_dump($dao->getAll());
        try {
            var_dump($dao->retrieve($id));
        } catch (Exception $ex) {
            echo "Une exception a été levé : <br>";
            echo $ex->getMessage();
            $this->updateLog($ex->getMessage());
        }
        
        // var_dump($dao->getAll());
        // $this->render("default");
    }

    /**
     * Simule un utilisateur qui se loggue.
     * On utilise l'objet {@see DefaultModel} qui retourne des valeurs par defaut
     * pour la generation du token. 
     * 
     * @link /login URI definie dans le fichier config/routing.json     * 
     * 
     */
    public function login() {
        $this->security->generateToken(new DefaultModel());
        header("Location: http://" . $_SERVER['SERVER_NAME'] . "/token");
    }

    /**
     * Simule un utilisateur qui se deconnecte.
     * La metode effectue une redirection 
     * 
     * 
     * @link /logout URI definie dans le fichier config/routing.json  
     * 
     */
    public function logout() {
        $this->security->deactivate();
        header("Location: http://" . $_SERVER['SERVER_NAME'] . "/token");
    }

    /**
     * Cette methode simule la verification du client
     * Affichage des informations du token pour s'assurer de l'identité. 
     *  
     * A tester apres s'etre connecté et a pres s'etre déconnecté afin de voir le comportement.
     * 
     * @link /token URI definie dans le fichier config/routing.json     * 
     * 
     */
    public function token() {
        var_dump($this->security->acceptConnexion());
    }

    /* Les methodes suivantes correspondent aux URI de test qui gèrent les verbes HTTP */
    
    /** 
     * Exemple d'utilisation avec la superglobale $_GET
     * 
     * @see Controller::inputGet() retourne la superglobale $_GET
     * 
     * @example /api/default/?test=petitMessage&key=valeur2 avec cette URI la methode retourne un tableau associatif correspondant aux données passées en arguments à l'URL
     */
    public function getDatasFromGET(){
        var_dump($this->inputGet());
    }
    
    /** 
     * Exemple d'utilisation avec la superglobale $_POST
     * 
     * @see Controller::inputPost() retourne la superglobale $_POST
     * 
     * @example /api/default ajouter dans le corps de la requete des données au format : x-www-form-urlencoded
     */
    public function getDatasFromPOST(){
        var_dump($this->inputPost());
    }
    
    /** 
     * Exemple d'utilisation avec la mise a jour d'une ressource via la methode PUT 
     * 
     * @see Controller::inputPut() retourne les données sous la forme d'un tableau associatif 
     * 
     * @example /api/default ajouter dans le corps de la requete des données au format : x-www-form-urlencoded
     */
    public function getDatasFromPUT(){
        var_dump($this->inputPut());
    }
    
    /** 
     * Ici la methode sera invoquée lors d'une requête HTTP dont le verbe est DELETE. 
     * L'exemple retourne les données des propriétés put, post et get. 
     * 
     * N'hésitez pas tester !
     */
    public function delete(){
        var_dump($this->inputPut());
        var_dump($this->inputPost());
        var_dump($this->inputGet());
    }
    
    /**
     * La methode affiche les données variables de l'URI comme definies dans le fichier routing.json. 
     * 
     * 
     * @param type $value correspond a la partie variable de l'URI dont le pattern est : (:).
     * 
     * @example /api/default/bonjour retournera bonjour. 
     * @example /api/default/32 retournera 32. 
     */
    public function getByValue($value){
        echo "valeur passée dans l'uri : " . $value;
    }

    /**
     * @return le resultat de l'opération si les arguments sont des nombres ou false dans le cas 
     * ou les arguments ne sont pas des nombres
     */
    public function addition ($a, $b) {
        if (!is_numeric($a) || !is_numeric($b)){return false;}
        return $a+$b;
    }

    public function soustratcion ($a, $b) {
        if (!is_numeric($a) || !is_numeric($b)){return false;}
        if ($a - $b < 0){return 0;}
        return $a-$b;
    }

    public function division ($a, $b) {
        if (!is_numeric($a) || !is_numeric($b)){return false;}
        if ($a == 0 || $b == 0){throw new Exception("impossible de diviser un nombre par 0 <br>");}
        return $a/$b;
    }
}
