<?php

use PHPUnit\framework\TestCase;
use  BWB\Framework\mvc\controllers\FizzBuzzController;

class FizzBuzzControllerTest extends TestCase {

    private $controller;

    // override la methode setUp de testCase
    public function setUp() :void {
        $this->controller = new fizzBuzzController();
    }

    public function testMultipleDe3() {
        $result = $this->controller->multipleDe3(3);
        $this->assertEquals("fizz", $result);
    }

    public function testMultipleDe5() {
        $result = $this->controller->multipleDe5(5);
        $this->assertEquals("buzz", $result);
    }

    public function testMultipleDe3Et5() {
        $result = $this->controller->multipleDe3Et5(15);
        $this->assertEquals("fizzbuzz", $result);
    }

    public function testPasMultipleDe3Et5() {
        $result = $this->controller->pasMultipleDe3Et5(4);
        $this->assertEquals("", $result);
    }

}