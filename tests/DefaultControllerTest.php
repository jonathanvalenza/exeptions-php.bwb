<?php

use PHPUnit\framework\TestCase;
use BWB\Framework\mvc\controllers\DefaultController;

class DefaultControllerTest extends TestCase {

    private $controller;

    // override la methode setUp de testCase
    public function setUp() :void {
        $this->controller = new DefaultController();
    }

    // on va tester si cette methode est bonne avec assertEquals()
    public function testAddition() {
        $controller = new DefaultController();
        $val1 = 10;
        $val2 = 20;
        $result = 0;
        $result = $controller->addition($val1, $val2);

        // assertEqual verifie si la première valeur passé en argument est egale à la seconde valeur passé en argument.
        $this->assertEquals(30, $result);
    }

    // on va tester si cette methode est mauvaise avec assertNotEquals()
    public function testNotAddition() {
        $controller = new DefaultController();
        $val1 = 10;
        $val2 = 20;
        $result = 0;
        $result = $controller->addition($val1, $val2);

        // assertNotEqual verifie si la première valeur passé en argument est différente de la seconde valeur passé en argument.
        $this->assertNotEquals(10, $result);
    }

    // on teste si cette methode retourne false. (spoiler : ça ne retourne pas)
    // en modifiant addition() avec des conditions, cela retournera false
    public function testAdditionErrorString () {
        $controller = new DefaultController();
        $val1 = "toto";
        $val2 = 20;
        $result = 0;
        $result = $controller->addition($val1, $val2);

        // assertFalse verifie si la valeur passé en argument retourn false
        $this->assertFalse($result);
    }
    // on va tester si cette methode est bonne avec assertEquals()
    public function testSoustraction() {
        $val1 = 20;
        $val2 = 10;
        $result = 0;
        $result = $this->controller->soustratcion($val1, $val2);
        $this->assertEquals(10, $result);
    }
    // on va tester si cette methode est bonne avec assertEquals()
    // dans le defaultController on a spécifier que si le résultat était inferieur a 0 (donc négatif) la methode dois retourner 0.
    public function testSoustractionNegatif() {
        $val1 = 10;
        $val2 = 20;
        $result = 0;
        $result = $this->controller->soustratcion($val1, $val2);
        $this->assertEquals(0, $result);
    }

    // on teste si cette methode retourne false. comme l'une des valeur n'est pas un nombre, la methode dois retourner false
    public function testNotSoustraction() {
        $val1 = "toto";
        $val2 = 10;
        $result = 0;
        $result = $this->controller->soustratcion($val1, $val2);
        $this->assertFalse($result);
    }

    // on va tester si cette methode est bonne avec assertEquals()
    public function testDivision() {
        $val1 = 20;
        $val2 = 10;
        $result = 0;
        $result = $this->controller->division($val1, $val2);
        $this->assertEquals(2, $result);
    }

    // on teste si cette methode retourne false. comme l'une des valeur n'est pas un nombre, la methode dois retourner false
    public function testNotdivision() {
        $val1 = "toto";
        $val2 = 10;
        $result = 0;
        $result = $this->controller->division($val1, $val2);
        $this->assertFalse($result);
    }

    // on teste si la methode retourne une exception. On a spécifier dans la methode du defaultController que si
    // l'une des valeur était 0, cela retournerai une exception. c'est ce que nous allons tester dans cette methode.
    public function testdivisionBy0() {
        $val1 = 0;
        $val2 = 10;
        $result = 0;

        // contrairement au assert, pour tester une exception, il faut ecrire ce code avant de tester le resultat.
        // si le resultat retourne une exception, alors le teste sera bon.
        $this->expectException(Exception::class);

        $result = $this->controller->division($val1, $val2);
        
    }
}