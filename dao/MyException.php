<?php

/**
 * Ici, on créer la classe qui servira de parent au différent classe d'exeption que l'on va implémenter
 * (ex isNotNumberException, SqlException...). Chaque classe devra traiter un type d'exception mais toutes
 * ce serviront du constructeur de MyException pour afficher dans le log les message d'erreur.
 */

class MyException extends Exception{
    public function __construct($message){
        //parent::__construct();
        $log = fopen("./log.txt", "a");
        fwrite($log, date("j-F-Y H:i:s : ") . $message);
        fclose($log);
    }
}