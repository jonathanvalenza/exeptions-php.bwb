<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\loggerException;
use Exception;

class SQLException extends loggerException {
public function __construct($message) {
    parent::__construct($message);
}
}
